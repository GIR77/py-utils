import copy
import json
import json2html
import pandas as pd
 
class JsonTable():
    def __init__(self):
        self.__titles = []
        self.items = []
        self.__pattern = {
            'type': 'TABLE',
            'settings': {
                'title': '',
                'cols': []
            },
            'data': []
        }

    def __repr__(self):
        return "<JsonTable with table(s): %s>" % ", ".join(self.__titles)

    def addData(self, title, data, columnNames=[]):
        if (data is None):
            raise Exception('Empty data')

        if (title is None) | (title == ''):
            raise Exception('Empty title')

        if title in self.__titles:
            raise Exception('This table already exist')

        self.__titles.append(title)

        table = copy.deepcopy(self.__pattern)
        table['settings']['title'] = title
        if type(data) == pd.DataFrame:
            table['data'] = json.loads(data.fillna('-').to_json(orient='records', force_ascii=False))
            cols = list(data.columns)
        else:
            table['data'] = json.loads(data)
            cols = list(table['data'][0].keys())

        columns = []
        for col in cols:
            columns.append({
                'name': col,
                'value': col
            })

        table['settings']['cols'] = columns
        self.items.append(table)
        
        if columnNames != []:
            self.setColumns(title, columnNames)
            
        return table
    
    def getListOfTables(self):
        return self.__titles
    
    def renameTable(self, title, newTitle):
        if self.__checkTable(title) == False:
            return
        
        table = self.getByTitle(title)
        for ttl in self.__titles:
            if ttl == title:
                ttl = newTitle
        
        table['settings']['title'] = newTitle

        print(f'Table {title} received a new name: "{newTitle}"')
        return table
    
    def dropTable(self, title):
        if self.__checkTable(title) == False:
            return
        table = self.getByTitle(title)
        self.items.remove(table)
        self.__titles.remove(title)
        
        print(f'Table {title} deleted.')
        return table
        
    def getColumns(self, title):
        if self.__checkTable(title) == False:
            return
        
        table = self.getByTitle(title)
        return table['settings']['cols']
    
    def getColumnsName(self, title):
        if self.__checkTable(title) == False:
            return
        
        table = self.getByTitle(title)
        columns = []
        
        for col in table['settings']['cols']:
            columns.append(col['name'])
        
        return columns
        
    def setColumns(self, title, columns):
        if self.__checkTable(title) == False:
            return
        
        table = self.getByTitle(title)
        if (type(columns) != list):
            raise Exception('The column list needs to be transferred as an array of column headers')
        
        if (len(columns) != len(table['settings']['cols'])):
            raise Exception(f'The number of columns in the table ({len (table ["settings"] ["cols"])}) Does not match the transferred set ({len (columns)}).')
        
        i = 0
        for col in table['settings']['cols']:
            col['name'] = columns[i]
            i+=1
        
        col_str = ', '.join(columns)
        print(f'Column headings are assigned to the table {title}: [{col_str}]')
              
        return table
        
    def getByTitle(self, title):
        for item in self.items:
            if item['settings']['title'] == title:
                return item
              
    
    def __checkTable(self, title):
        if self.getByTitle(title) is None:
            raise Exception('Table not found')

        return True
              
    def getJson(self):
        return json.dumps(self.items, ensure_ascii=False)

    def getHTML(self):
        html = ''
        for item in self.items:
            collist = list(map(lambda x: {x['value']: x['name']}, item['settings']['cols']))
            cols = {}
            for col in collist:
                cols.update(col)

            df = pd.DataFrame(item['data']).rename(columns=cols)
            htmltable = json2html.json2html.convert(json=df.to_json(orient='records', force_ascii=False), table_attributes='')
            html = html + f"<p><b>{item['settings']['title']}</b>{htmltable}</p>"
        return html

    @classmethod
    def readJson(cls, json_str:str):
        json_objs = json.loads(json_str)
        result = cls()
        for json_obj in json_objs:
            if 'type' not in json_obj:
                raise Exception('Have not a type')

            if json_obj.get('type', '').lower() != 'table':
                raise Exception('Type is not a table. Type: %s' % json_obj.get('type', ''))

            settings = json_obj.get('settings', dict())

            title = settings.get('title', 'No title')
            cols = settings.get('cols', [])
            if cols != []:
                cols = list(map(lambda x: x.get('name', x), cols))

            data = json_obj.get('data', [])
            data = json.dumps(data)

            result.addData(title, data, columnNames=cols)

        return result


    @classmethod
    def wrap(cls, data, title='Таблица', columnNames=None):
        jt = cls()
        jt.addData(title, data, columnNames)
        return jt.getJson()


if __name__ == '__main__':
    import json2html
    jt = JsonTable()

    jt.addData(
        'Table Name 01',
        pd.DataFrame({'Column1': [0, 1, 2], 'Column2': ['0', '1', '2']}),
        columnNames=['Колонка с цифрами', 'Колонка со строками'])
    jt.addData(
        'Table Name 02',
        pd.DataFrame({'Col1': [0, 1, 2], 'Col2': ['0', '1', '2']}),
        columnNames=['Колонка с цифрами', 'Колонка со строками'])


    json_str = jt.getJson()

    print(jt.getHTML())
